export const ENDPOINTS = {
    home: 'http://localhost:3000/home',
    list: 'http://localhost:3000/sillones',
    about: 'http://localhost:3000/about',
    detail: 'http://localhost:3000/detail/',
}