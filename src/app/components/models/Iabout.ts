export interface Designers {
    id: number | string;
    name: string;
    alterego: string;
    description: string;
    img: string;
}
