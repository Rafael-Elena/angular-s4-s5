export interface Sillones {
    id: number | string;
    img: Img;
    name: string;
    type: string;
    price: number;
    stock?: string;
 }

export interface Img {
     url: string;
     alt: string;
 }

