export interface Home {
    id: number;
    title: string;
    description: string;
}