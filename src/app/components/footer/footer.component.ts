import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  public legales: string;
  public contact: string;
  constructor() {
    this.legales= 'copyright 2020';
    this.contact= 'info@avr.es';
   }

  ngOnInit(): void {
  }

}
