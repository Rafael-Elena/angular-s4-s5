import { Component, OnInit } from '@angular/core';

import { HomeService } from './../services/home.service';
import { Home } from './../../../components/models/Ihome';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  public title: string;
  public home: Home[];

  constructor(private homeService: HomeService) {
    this.title = '';
  }


  ngOnInit(): void {
    this.getListHome();
  }

  public getListHome(): void {
    this.homeService.getHome().subscribe(
      (data: Home[]) => {
        this.home = data;
      },
      (error) => {
        console.error(error.message);
      }
    );
  }

}
