import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

import { ENDPOINTS } from '../../../../endPoints/endPoints';
import { Home } from './../../../components/models/Ihome';

@Injectable({
  providedIn: 'root'
})

export class HomeService {
  private homeUrl: string = ENDPOINTS.home;

  constructor(private httpClient: HttpClient) {
    /*Empty*/
  }

  public getHome(): Observable<any> {
    return this.httpClient.get(this.homeUrl).pipe(
      map((response: Home) => {
        if (!response) {
          throw new Error('Value Expected');
        } else {
          return response;
        }
      }),
      catchError((error) => {
        throw new Error(error.mesage);
      })
    );
  }
}
