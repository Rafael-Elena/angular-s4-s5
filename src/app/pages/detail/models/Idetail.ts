export interface Detail {
    id: number;
    img: Img;
    name: string;
    type: string;
    price: number;
    stock?: string;
    description: string;
}

export interface Img {
    url: string;
    alt: string;
}
