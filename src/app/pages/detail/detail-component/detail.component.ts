import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { DetailService } from './../services/detail.service';
import { Detail } from '../models/Idetail';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailComponent implements OnInit {
  public sillonDetail: Detail;
  private sillonId: string;
  constructor(
    private detailService: DetailService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.getDetail();
  }

  public getDetail(): void {
    this.route.paramMap.subscribe((params) => {
      this.sillonId = params.get('id');
    });
    this.detailService.getDetailSillon(Number(this.sillonId)).subscribe(
      (data: Detail) => {
        this.sillonDetail = data;
      },
      (error) => {
        console.error(error.message);
      }
    );
  }
}
