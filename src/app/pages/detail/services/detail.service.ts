import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';


import { ENDPOINTS } from '../../../../endPoints/endPoints';
import { Detail } from './../models/Idetail';

@Injectable({
  providedIn: 'root'
})
export class DetailService {
  private detailUrl: string = ENDPOINTS.detail;

  constructor(private httpClient: HttpClient) {
    /*Empty*/
  }

  public getDetailSillon(id: number): Observable<Detail> {
    return this.httpClient.get(this.detailUrl + id).pipe(
      map((response: Detail) => {
        if (!response) {
          throw new Error('Value Expected');
        } else {
          return response;
        }
      }),
      catchError((error) => {
        throw new Error(error.message);
      })
    );
  }
}
