import { Component, OnInit } from '@angular/core';

import { Sillones } from 'src/app/components/models/Iproduct';
import { ComunicationService } from './../../../services/comunication.service';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class ListComponent implements OnInit {
  public title: string;
  public sillones: Sillones[];


  constructor(private comunicationService: ComunicationService) {
    this.title = 'Listado de sillones';
  }

  ngOnInit(): void {
    this.getListSillon();
  }

  public getListSillon(): void {
    this.comunicationService.getSillon().subscribe((data: Sillones[]) => {
      this.sillones = data;
    }, (error) => {
      console.error(error.message);
    });
  }
}
