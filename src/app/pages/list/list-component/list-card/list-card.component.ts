import { Component, Input, OnInit } from '@angular/core';

import { Detail } from './../../../detail/models/Idetail';

@Component({
  selector: 'app-list-card',
  templateUrl: './list-card.component.html',
  styleUrls: ['./list-card.component.scss']
})
export class ListCardComponent implements OnInit {
  @Input() public sillonDetail: Detail;
  constructor() { }

  ngOnInit(): void {
  }

}
