import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListRoutingModule } from './list-routing.module';
import { ListComponent } from './list-component/list.component';
import { ListCardComponent } from './list-component/list-card/list-card.component';


@NgModule({
  declarations: [ListComponent, ListCardComponent],
  imports: [
    CommonModule,
    ListRoutingModule
  ]
})
export class ListModule { }
