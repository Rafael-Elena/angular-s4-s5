import { ComunicationService } from './../../../services/comunication.service';
import { Sillones } from './../../../components/models/Iproduct';

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
})
export class CreateComponent implements OnInit {
  public title: string;
  public createSillon: FormGroup | null;
  public okey: boolean = false;
  public reg = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?';
  public creatorList: Sillones;
  public idCounter: number = 10;

  constructor(
    private formBuilder: FormBuilder,
    private comunicationService: ComunicationService,
    ) {
    this.title = 'Envía tu propuesta de sillón';
  }

  ngOnInit(): void {
   this.createDesign();
  }

  public createDesign(): void {
    this.createSillon = this.formBuilder.group({
      url: ['', [Validators.required, Validators.pattern(this.reg)]],
      alt: ['', [Validators.required, Validators.minLength(3)]],
      name: ['', [Validators.required, Validators.minLength(3)]],
      type: ['', [Validators.required, Validators.minLength(3)]],
      price: ['', [Validators.required, Validators.min(50)]],
      id: [this.idCounter, []],
      stock: ['new', []],
    });
  }

 public sillonCreate(): void {
   const mySillon: Sillones = {
      id: this.createSillon.get('id').value,
      img:
      {
       url: this.createSillon.get('url').value,
       alt: this.createSillon.get('alt').value,
      },
      name: this.createSillon.get('name').value,
      type: this.createSillon.get('type').value,
      price: this.createSillon.get('price').value,
      stock: this.createSillon.get('stock').value,
    };

   this.comunicationService.postProduct(mySillon).subscribe((data) => {
        console.log(data);
      }, (error) => {
       console.error(error.message);
      }
    );
  }

  public onSubmit(): void {
    this.okey = true;
    if (this.createSillon.valid) {
      this.sillonCreate(),
      this.createSillon.reset();
      this.okey = false;
      this.title = '¡Ya puedes ver tu producto publicado en el listado!';
      this.idCounter = this.idCounter + 1;
    }
  }
}
