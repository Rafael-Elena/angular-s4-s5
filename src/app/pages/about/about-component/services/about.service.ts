import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


import { ENDPOINTS } from './../../../../../endPoints/endPoints';
import { Designers } from './../../../../components/models/Iabout';

@Injectable({
  providedIn: 'root',
})
export class AboutService {
  private designersUrl: string = ENDPOINTS.about;

  constructor(private httpClient: HttpClient) {
    /*Empty*/
  }

  public getDesigners(): Observable<any> {
    return this.httpClient.get(this.designersUrl).pipe(
      map((response: Designers[]) => {
        if (!response) {
          throw new Error('Value Expected');
        } else {
          return response;
        }
      }),
      catchError((error) => {
        throw new Error(error.mesage);
      })
    );
  }
}
