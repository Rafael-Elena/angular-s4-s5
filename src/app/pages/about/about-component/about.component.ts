import { Component, OnInit } from '@angular/core';

import { AboutService } from './services/about.service';
import { Designers } from './../../../components/models/Iabout';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
})

export class AboutComponent implements OnInit {
  public title: string;
  public designers: Designers[];

  constructor(private aboutService: AboutService) {
    this.title = 'Equipo de diseño';
  }

  ngOnInit(): void {
    this.getListAbout();
  }

  public getListAbout(): void {
    this.aboutService.getDesigners().subscribe(
      (data: Designers[]) => {
        this.designers = data;
      },
      (error) => {
        console.error(error.message);
      }
    );
  }
}
