import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';


import { ENDPOINTS } from './../../endPoints/endPoints';
import { Sillones } from './../components/models/Iproduct';

@Injectable({
  providedIn: 'root',
})
export class ComunicationService {
  private sillonesUrl: string = ENDPOINTS.list;

  constructor(private httpClient: HttpClient) {
    /*Empty*/
  }

  public getSillon(): Observable<Sillones[]> {
    return this.httpClient.get(this.sillonesUrl).pipe(
      map((response: Sillones[]) => {
        if (!response) {
          throw new Error('Value Expected');
        } else {
          return response;
        }
      }),
      catchError((error) => {
        throw new Error(error.message);
      })
    );
  }

  public postProduct(sillon: Sillones): Observable<Sillones> {
    return this.httpClient.post(this.sillonesUrl, sillon).pipe(
      map((response: Sillones) => {
        if (!response) {
          throw new Error('Value Expected');
        } else {
          return response;
        }
      }),
      catchError((error) => {
        throw new Error(error.message);
      })
    );
  }
}
